<?php
  require_once 'connection.php';
  require_once 'scrape.php';

  ini_set('max_execution_time', 0);
  ini_set('memory_limit', '512M');

  if(isset($_POST['inpVal'])) {
    
    $bot = new Bot();
    $bot->index($_POST['inpVal']);
    
  }
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/style.css">
  <title>URL</title>
</head>
<body>

  <div class="container">
    <input type="text" class="inp" placeholder="Type URL here ...">
    <button class="btn">Search</button>
  </div>
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="/js/script.js"></script>
</body>
</html>