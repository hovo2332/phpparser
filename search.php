<?php

  require_once "scrape.php";

  if(isset($_POST['inp'])) {
      
    $bot = new Bot();
    $bot->search($_POST['inp']);
    
  }    

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/style.css">
  <title>Search</title>
</head>
<body>

  <form action="search.php" class="container" method="POST">
    <input type="text" name="inp" class="searchInp" placeholder="Type URL here ...">
    <button class="btn">Search</button>
  </form>
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="/js/script.js"></script>
</body>
</html>