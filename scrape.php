<?php

  class Bot
  {
    public $arr = [];
    public $matched = [];

    private function isInternal($link, $url) {
      $url_parts  = parse_url($url);
      $link_parts = parse_url($link);
        if (isset($link_parts['host'])) {
            if ($link_parts['host'] == $url_parts['host']) {
                return substr($link, 0, 2) == '//' ? $url_parts['scheme'] . ':' . $link : $link;
            }
        } else if ($link_parts['path'][0] == '/' && strlen($link_parts['path']) > 1) {
            return $url_parts['scheme'] . '://' . $url_parts['host'] . $link;
        }
        return false;
    }

    private function strip_tags_content($text) {
      return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
  }
    public function index($url)
    {
      if(!in_array($url, $this->matched)) {
        $this->matched[] = $url;
      }

      
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $html = curl_exec($ch);

      $dom = new DOMDocument();
      libxml_use_internal_errors(true);
      $dom->loadHTML($html);
      libxml_clear_errors();

      $links = $dom->getElementsByTagName('a');
            
      foreach ($links as $link) {
          
        
        $href = $this->isInternal($link->getAttribute('href'),$url);
        
        $content = $this->strip_tags_content($link->nodeValue);
        
        if($href && !in_array($href, $this->matched)) {
          
          $this->matched[] = $href;
          
          $date = date("Y-m-d H:i:s");
          mysqli_query(new mysqli("localhost", "root", "root", "parser"),"INSERT INTO links (referrer, href, content, created_at) VALUES ('$url', '$href', '$content', '$date')");
          $this->index($href); 
        }
      }
    }

    public function search($ref,$level = 0) {

      $referrers = mysqli_query(new mysqli("localhost", "root", "root", "parser"), "SELECT href FROM links WHERE links.referrer = '$ref'");
     
      if($result = $referrers) {
        while($row = $result->fetch_assoc()) {
          echo str_repeat("&nbsp;", $level); 
          echo "<a href='{$row['href']}'>".$row['href']."</a>" . "<br>";
          $this->search($row['href'], $level + 10);
        }
      }
    }
  }
